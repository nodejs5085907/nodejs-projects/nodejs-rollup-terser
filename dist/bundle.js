(function () {
    'use strict';

    function moduleOne() {
        console.log('Module One works!');
    }

    function moduleTwo() {
        console.log('Module Two works!');
    }

    moduleOne();
    moduleTwo();

})();
